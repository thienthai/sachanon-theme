<?php
    /**
     * Enqueue scripts and styles.
     */
    function sachanon_scripts() {
        wp_enqueue_style( 'sachanon-style', get_stylesheet_uri(), array(), wp_get_theme()->get( 'Version' ) );

        wp_enqueue_style( 'sachanon-main-style', get_template_directory_uri() . '/css/main.css', array(), wp_get_theme()->get( 'Version' ));
    }

    add_action( 'wp_enqueue_scripts', 'sachanon_scripts' );

?>